
-- TODO : mettre en full text le champ recherchés.
CREATE DATABASE IF NOT EXISTS job_board CHARACTER SET 'utf8';

USE job_board;
CREATE TABLE IF NOT EXISTS `users` (
  `id` SMALLINT AUTO_INCREMENT ,
  `email` VARCHAR(64) UNIQUE NOT NULL,
  `first_name` VARCHAR(64) NOT NULL,
  `second_name` VARCHAR(64) NOT NULL,
  `password` CHAR(60) BINARY,
  `phone_number` VARCHAR(15) NOT NULL,
  `role` TINYINT(1) NOT NULL,
  `image_url` VARCHAR(350), 
  PRIMARY KEY (`id`), 
  FULLTEXT INDEX ind_full_email (`email`)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `companies` (
  `id` SMALLINT AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL,
  `admin_id` SMALLINT  NOT NULL,
  `image_url` VARCHAR(350),
  `description` TEXT,
  `siren` VARCHAR(9),
  `naf` VARCHAR(10),

  PRIMARY KEY (`id`),

  CONSTRAINT fk_admin_id
    FOREIGN KEY (`admin_id`)
    REFERENCES users(`id`) ON DELETE CASCADE ON UPDATE CASCADE

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `advertisments` (
  `id` SMALLINT AUTO_INCREMENT ,
  `company_id` SMALLINT NOT NULL,
  `title` VARCHAR(100), 
  `excerpt` VARCHAR (350),
  `full_description` TEXT,
  `wages` DECIMAL(19,4),
  `working_time` DECIMAL(5,2),
  `start_date` DATETIME,
  `working_place` VARCHAR(350),

  PRIMARY KEY (`id`),
  CONSTRAINT fk_company_id
    FOREIGN KEY (`company_id`)
    REFERENCES companies(`id`) ON DELETE CASCADE ON UPDATE CASCADE 

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- CREATE TABLE IF NOT EXISTS `tags` (
--   `id` SMALLINT AUTO_INCREMENT ,
--   `name` VARCHAR(20),

--   PRIMARY KEY (`id`)

-- )
-- ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- CREATE TABLE IF NOT EXISTS `tag_relationships` (
--   `advertisment_id` SMALLINT NOT NULL,
--   `tag_id` SMALLINT NOT NULL,

--   PRIMARY KEY (`advertisment_id`,`tag_id`),
--   CONSTRAINT fk_advertisment_id
--     FOREIGN KEY (`advertisment_id`)
--     REFERENCES advertisments(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   CONSTRAINT fk_tag_id
--     FOREIGN KEY (`tag_id`)
--     REFERENCES tags(`id`) ON DELETE CASCADE ON UPDATE CASCADE

-- )
-- ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- CONTIENT LES CANDIDATURES au quelles peuvent être associées un ou plusieurs mails
CREATE TABLE IF NOT EXISTS `applications` (
  `applicant_id` SMALLINT NOT NULL,
  `advertisment_id` SMALLINT NOT NULL,

  FOREIGN KEY (`applicant_id`)
  REFERENCES users(`id`) ON DELETE CASCADE ON UPDATE CASCADE ,
  
  FOREIGN KEY (`advertisment_id`)
  REFERENCES advertisments(`id`) ON DELETE CASCADE ON UPDATE CASCADE ,

  -- KEY COMPOSITE
  PRIMARY KEY (`applicant_id`,`advertisment_id`)


)
ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `mails` (
  `id`  SMALLINT NOT NULL AUTO_INCREMENT,
  `applicant_id` SMALLINT NOT NULL,
  `advertisment_id` SMALLINT NOT NULL,
  `is_from_applicant` TINYINT(1) NOT NULL,
  `content` TEXT,
  `date` DATETIME NOT NULL,

  PRIMARY KEY (`id`),

  CONSTRAINT fk_mails_applicant_id
    FOREIGN KEY (`applicant_id` )
    REFERENCES applications( `applicant_id`)ON DELETE CASCADE ON UPDATE CASCADE ,
  CONSTRAINT fk_mails_advertisment_id
    FOREIGN KEY (`advertisment_id` )
    REFERENCES applications( `advertisment_id`)ON DELETE CASCADE ON UPDATE CASCADE 

) 
ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `users` (`id`,`email`,`first_name`, `second_name`, `phone_number`, `password`, `role`) VALUES 
(1, 'super_admin@gmail.com', 'Zuper', 'Admin','0660503272','$2b$10$42rKNbepcaA3tAtT/rMpGO.XULHts2fQ/kLHuIlHyd4DoYHItEvo.', 2),
(2, 'maurice_dupont@gmail.com', 'Maurice', 'Dupont','0660503272','$2b$10$42rKNbepcaA3tAtT/rMpGO.XULHts2fQ/kLHuIlHyd4DoYHItEvo.',1),
(3, 'michel_dupond@gmail.com', 'Michel', 'Dupond','0660503272','$2b$10$42rKNbepcaA3tAtT/rMpGO.XULHts2fQ/kLHuIlHyd4DoYHItEvo.', 0),
(4, 'beatrice_smecta@gmail.com', 'Beatrice', 'Smecta','0660503272','$2b$10$42rKNbepcaA3tAtT/rMpGO.XULHts2fQ/kLHuIlHyd4DoYHItEvo.', 0),
(5, 'fortza_comerziale@gmail.com', 'Antonio', 'Fortza','0660503272','$2b$10$42rKNbepcaA3tAtT/rMpGO.XULHts2fQ/kLHuIlHyd4DoYHItEvo.', 1),
(6, 'en_galere@gmail.com', 'Désir', 'Unemploi','0660503272','$2b$10$42rKNbepcaA3tAtT/rMpGO.XULHts2fQ/kLHuIlHyd4DoYHItEvo.', 0),
(7, 'baravalle.sebastien@gmail.com', 'Sébastien', 'Baravalle','0660503272','$2b$10$42rKNbepcaA3tAtT/rMpGO.XULHts2fQ/kLHuIlHyd4DoYHItEvo.',1 )
;

INSERT INTO `companies` (`id`,`name`,`admin_id`, `image_url`, `description`) VALUES
(1, 'Ubisoft', 1, 'https://www.challenges.fr/assets/img/2017/05/31/cover-r4x3w1000-592e8a4257fff-capture.jpg', 'mais bien sur etcCupcake ipsum dolor sit amet carrot cake cupcake. Toffee marshmallow sweet roll cake I love jujubes cheesecake powder biscuit. Chocolate bar liquorice gummies gummi bears apple pie shortbread. Bear clops lollipop wafer toffee toffee. Fruitcake marshmallow sesame snaps sesame snaps jelly-o macaroon chocolate. Marshmallow cookie gingerbread shortbread I love dragée I love toffee. Danish liquorice I love dragée topping I love. Jelly beans dessert candy canes shortbread macaroon gummies marzipan.' ),
(2, 'Pointcom.com', 2, 'https://image.freepik.com/photos-gratuite/lapin-mignon-fond-blanc_1232-462.jpg', 'srdtfyuguihiojopkp')
;

INSERT INTO `advertisments` (`id`,`company_id`,`title`, 
`excerpt`,
`full_description`,
`wages`,
`working_time`,
`start_date`,
`working_place`
) VALUES 
(1,  1, 'Annonce Ubisoft recrute',
'BLABLA et Balabalna',
'Une grande entreprise etc recherche etc un dev pour etc etcetctecegtcetectecetc.'
, 1599.99, 35.00, '2021-12-19 00:24:10', "la bas"  ),
(2, 2, 'Pointcom.com recrute ',
'BLABLA et Balabalna',
'Une grande entreprise etc recherche etc un dev pour etc etcetctecegtcetectecetc.'
, 1599.99, 35.00, '2021-12-19 00:24:10', "ici"  
)
;

-- INSERT INTO `tags` (`id`,`name`) VALUES
-- (1,"dev"), 
-- (2,"game_dev"); 

-- INSERT INTO `tag_relationships` (`advertisment_id`,`tag_id`) VALUES
-- (1,1), 
-- (1,2), 
-- (2,1); 

INSERT INTO `applications` ( `applicant_id`,`advertisment_id`) VALUES 
( 3 , 1), -- MICHEL repond à  Ubisoft
( 3 , 2), -- MICHEL repond à  Pointcom.com
( 4 , 2)  -- BEATRICE repond à Pointcom.com
;


INSERT INTO `mails` (`id`,`applicant_id` , `advertisment_id`, `is_from_applicant` ,`content`,`date`) VALUES 
(1, 3, 1,1,  'Bonjour moi Michel postule à Ubisoft', '2021-09-29 12:24:10'),
(2, 3,2, 1, 'Bonjour moi Michel postule à Pointcom.com', '2021-09-30 13:21:40'),
(3, 4,2, 1, 'Bonjour moi Beatrice pour Pointcom.com', '2021-10-11 13:21:40')
;

-- -- va chercher mails de michel 
-- SELECT mails.content FROM mails INNER JOIN applications ON mails.applicant_id = applications.applicant_id AND mails.advertisment_id = applications.advertisment_id WHERE applications.applicant_id = 3;

-- -- Insertion avec donnée d'autre table
-- BEGIN;
-- INSERT INTO companies (name, admin_id)
-- SELECT 'FortzaComerziale',id AS admin_id FROM users 
-- WHERE users.second_name = 'Fortza';
-- INSERT INTO advertisments (company_id, title )
--   VALUES(LAST_INSERT_ID(), "Fortza recrute !!! ");
-- COMMIT;

-- -- -- Insertion avec donnée d'autre table
-- BEGIN;
-- INSERT INTO applications (applicant_id, advertisment_id)
-- SELECT 6,  @var_advert_id :=  id AS advertisment_id FROM advertisments 
-- WHERE company_id = (SELECT id FROM companies WHERE admin_id = 5 );
-- INSERT INTO mails (applicant_id, advertisment_id, content, date, is_from_applicant  )
--   VALUES( 6, @var_advert_id, "Je me sens quasiment à la hauteur.", NOW(), 1);
-- COMMIT;



-- EXEMPLE JOINTURE MULTIPLE
-- TODO: l'exemple ne tient pas compte de la modification (clef composite sur mail)
-- SELECT 
-- users.first_name as Candidat , companies.name as Company , advertisments.title as Annonce , Admin.first_name as Admin, mails.content as Message
-- FROM users 
-- INNER JOIN applications
--   ON  users.id = applications.applicant_id 
-- INNER JOIN advertisments
--   ON  applications.advertisment_id = advertisments.id
-- INNER JOIN companies
--   ON  advertisments.company_id = companies.id
-- INNER JOIN users as Admin
--   ON  companies.admin_id = Admin.id
-- INNER JOIN mails
--   ON  applications.id = mails.id
-- WHERE applications.advertisment_id > 0;

-- SHOW TABLES; 

-- DESCRIBE users; 
-- SELECT * FROM users;
--  recherche naturelle par defaut : renvoie mots qui contient lettre corespondantes
-- SELECT * FROM users WHERE MATCH(email) AGAINST('gmail');

-- DESCRIBE companies; 
-- Foreign key empeche d'ajouter liaison vers id_admin innexistant
-- INSERT INTO companies (`name`,`admin_id`) VALUES ('haricots.com', 5); 
-- SELECT * FROM companies;
-- DESCRIBE advertisments; 
-- SELECT * FROM advertisments;


-- DESCRIBE mails; 
-- SELECT * FROM mails;

-- DESCRIBE applications; 
-- SELECT * FROM applications;


-- DROP DATABASE IF EXISTS job_board;