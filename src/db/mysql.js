const mysql = require('mysql2')

const db_consts = require('../../db_consts')

const pool = mysql.createPool({
  connectionLimit : 10,
  multipleStatements: true,
  host  :  'localhost',
  user: db_consts.user,
  password: db_consts.password,
  database: 'job_board'
})



module.exports = pool