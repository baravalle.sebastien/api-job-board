const EntityManager = require("./entityManager");
module.exports = class UserManager extends EntityManager {
  constructor(req, res) {
    super(req, res)
    this.entity_name = 'advertisment'
    this.authorisedDisplayabledFields = ["*, advertisments.id as advertisment_id"]
  }

  // TODO GetAll get By Id et get by search sont le smeme que l'entity manager avec une lisaison en plus, -> inclure mecanime pour ajouter de methode du querymaker entre

  getAll(fieldsWeWant = this.authorisedDisplayabledFields) {
    this.queryMaker = this.queryMaker.init(this.req)
      .selectFieldsOn(fieldsWeWant, this.entity_name + 's')
      .join([
        ["companies", "advertisments.company_id = companies.id"]
      ]).order("advertisments.id", "DESC")
      .limitAndOffset("10", `${this.pool.escape(this.getOffset(this.req.query))}`)
      .close()
    return this
  }
  getMy(fieldsWeWant = this.authorisedDisplayabledFields) {
    this.values = this.inputParser.instantiate(this.req, this.pool)
      .setUser(this.req.user, this.req.query.user_id)
      .giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .selectFieldsOn(['*, advertisments.id AS advertisment_id'], "advertisments")
      .join([
        ["companies", "advertisments.company_id = companies.id"]
      ])
      .where([
        [`companies.admin_id = :user_id`, ""]
      ]).close()
    return this
  }

  getById(fieldsWeWant = this.authorisedDisplayabledFields) {
    const entity = this.entity_name
    let object_to_save = {}
    object_to_save[entity + '_id'] = this.req.params[entity + '_id']
    let values = this.inputParser.instantiate(this.req, this.pool)
      .saveProps(object_to_save).giveValues()

    this.queryMaker = this.queryMaker.init(this.req, values)
      .selectFieldsOn(['*, advertisments.id AS advertisment_id'], "advertisments")
      .join([
        ["companies", "advertisments.company_id = companies.id"]
      ])
      .where([
        [`advertisments.id = :advertisment_id`, ""]
      ]).close()


    return this

  }
  searchOnStr(column, fieldsWeWant = this.authorisedDisplayabledFields) {

    this.queryMaker = this.queryMaker.init(this.req)
      .selectFieldsOn(['*, advertisments.id AS advertisment_id'], this.entity_name + 's')
      .join([
        ["companies", "advertisments.company_id = companies.id"]
      ])
      .where([
        [`${this.mapStrSearch(this.req.query, this.pool, this.entity_name, column)}`, '']
      ]).limitAndOffset("10").close()
    return this
  }

  post() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .parseImgPath(this.req.file, 'companies', "companies.name", false, "companies.image_url")
      .setUser(this.req.user, this.req.body.user_id)
      .saveProps({
        company_id: this.req.body.company_id
      })
      .controlKeys("advertisment").controlKeys("company")
      .mapForInsert("advertisment")
      .deleteProperties(["company_id"])

      .mapForInsert("company")
      .giveValues()

    // CREATE ADVERTISMENT : 
    /**
     * C'est un cas particulier car advertisment est liée a une companie
     * Donc il faut en premier lier la companie
     *  I.    Si l'utilisateur est authorisé, la lier a une companie existante
     *  OU
     *  II.   Si elle n'existe pas, la creer
     */

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .onCondition(
        'company_id',
        (company_exist) => {    //  I. un id de companie existante est communiqué afin de la lier aux nouvel advertisment.

          return company_exist
            .verifyPropriety('company')

        },
        (create_company) => {   //  II. le parametre de la compagnie est inexistant donc il faut la creer:
          return create_company.isAdmin()
            .insert("company",
              '\`admin_id\`,',
              '@is_admin, '
            ).close()
            .setVariables([
              ['@company_id', 'LAST_INSERT_ID()']
            ])
        }
      )
      .insert('advertisment',
        `\`company_id\`,`,
        `@company_id , `
      )
      .close()

    return this
  }

  patch() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
    if (this.req.user.role === 2) this.values.advertisment.push("company_id")   // Sert a super Admin de lier annonce a nouvelle companie.

    this.values = this.values.setUser(this.req.user, this.req.body.user_id)
      .saveProps({
        advertisment_id: this.req.params.advertisment_id
      }).controlKeys("advertisment")
      .mapForUpdate("advertisment")
      .giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .verifyPropriety('advertisment')
      .updateById('advertisment', '@advertisment_id').close()

    return this
  }

  deleteById() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.query.user_id)
      .saveProps({
        advertisment_id: this.req.params.advertisment_id
      })
      .giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .verifyPropriety('advertisment')
      .deleteById(this.entity_name, '@advertisment_id').close()

    return this

  }
}