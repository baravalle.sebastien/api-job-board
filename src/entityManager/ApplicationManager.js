const EntityManager = require("./entityManager");
module.exports = class ApplicationManager extends EntityManager {
  constructor(req, res) {
    super(req, res)
    this.entity_name = 'application'


    this.authorisedDisplayabledFields = [
      "applications.advertisment_id",
      "applications.applicant_id", "companies.admin_id as admin_id",
      "applicant.first_name as applicant_first_name",
      "applicant.email as applicant_email",
      "applicant.second_name as applicant_second_name",
      "applicant.id as applicant_id",
      "admin.first_name as admin_first_name",
      "admin.second_name as admin_second_name",
      "admin.email as admin_email",
      "advertisments.title as advertisment_title",
      "companies.name as company_name",

    ]
  }
  getAll(fieldsWeWant = this.authorisedDisplayabledFields) {
    this.values = this.inputParser
      .instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.body.user_id)
      .giveValues()


    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .selectFieldsOn(fieldsWeWant, this.entity_name + 's')
      .join([			// Joint la candidature avec le candidat et l'admin 
        ["advertisments", "applications.advertisment_id = advertisments.id"],
        ["companies", "advertisments.company_id = companies.id"],
        ["users as applicant", "applications.applicant_id = applicant.id"],
        ["users as admin", "companies.admin_id = admin.id"]
      ])
      .where([
        [`applicant_id = :user_id`, 'OR'],
        [` companies.admin_id = :user_id`, '']
      ]).close()

    return this

  }

  getByAdvertisment(fieldsWeWant = this.authorisedDisplayabledFields) {
    this.values = this.inputParser
      .instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.query.user_id)
      .saveProps({
        advertisment_id: this.req.params.advertisment_id
      }).giveValues()

    // console.log('Values ', this.values);

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .verifyPropriety('advertisment')
      .selectFieldsOn(fieldsWeWant, this.entity_name + 's')
      .join([			// Joint la candidature avec le candidat et l'admin 
        ["advertisments", "applications.advertisment_id = advertisments.id"],
        ["companies", "advertisments.company_id = companies.id"],
        ["users as applicant", "applications.applicant_id = applicant.id"],
        ["users as admin", "companies.admin_id = admin.id"]
      ])
      .where([
        [`advertisments.id = @advertisment_id`, '']
      ]).close()

    return this


  }

  post() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.body.user_id)
      .saveProps({
        advertisment_id: this.req.body.advertisment_id
      })
      .deleteProperties(["applicant_id", "advertisment_id"])
      .addProperties({ is_from_applicant: 1, date: new Date() }).controlKeys("mail")
      .mapForInsert("mail").giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .isNotAdmin()
      .insert(
        "application",
        "\`applicant_id\`,\`advertisment_id\`",
        `@is_not_admin, :advertisment_id `
      ).close()
      .insert(
        "mail",
        `\`applicant_id\`,\`advertisment_id\`, `,
        `@is_not_admin ,  :advertisment_id,  `
      ).close()

    return this

  }
  deleteByadvertisment_andCurrentUser() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.query.user_id)
      .saveProps({
        advertisment_id: this.req.params.advertisment_id
      }).giveValues()


    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .setVariables([
        ['@advertisment_id', 'NULL'],
        ['@applicant_id', 'NULL']
      ])
      .selectFieldsOn([
        "@applicant_id :=  applications.applicant_id",
        "@advertisment_id := applications. advertisment_id"
      ], "applications"
      )
      .where([
        [`applications.advertisment_id = :advertisment_id`, "AND"],
        [`applications.applicant_id = :user_id `, '']
      ]).close()
      .delete('applications').where([
        [`applications.applicant_id = @applicant_id`, "AND"],
        [`applications.advertisment_id = @advertisment_id`, ""]
      ]).close()
    return this
  }
}
