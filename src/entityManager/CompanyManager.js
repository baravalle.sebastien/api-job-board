const EntityManager = require("./entityManager");

module.exports = class CompanyManager extends EntityManager {
  constructor(req, res) {
    super(req, res)
    this.entity_name = 'company'
    this.tableName = 'companies'
  }

  getMyCompanies() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool).setUser(this.req.user, this.req.query.user_id)
      .giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .selectFieldsOn(["*"], 'companies')
      .where([
        ["admin_id = :user_id", ""]
      ]).close()

    return this

  }

  post() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.body.user_id)
      .parseImgPath(this.req.file, 'companies', 'companies.name', false, "companies.image_url")
      .controlKeys('company')
      .mapForInsert("company")
      .giveValues()


    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .isAdmin()
      .insert('company',
        'admin_id , ',
        '@is_admin , '
      ).close()

    return this

  }

  patch() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.body.user_id)
      .saveProps({
        company_id: this.req.params.company_id
      })
      .parseImgPath(this.req.file, 'companies', 'companies.name', false, "companies.image_url")
      .controlKeys('company')
      .mapForUpdate("company")
      .giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .verifyPropriety('company')
      .selectById(['image_url'], 'company').close()
      .updateById('company').close()

    return this
  }

  deleteById() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.query.user_id)
      .saveProps({
        company_id: this.req.params.company_id
      })
      .giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .verifyPropriety('company')
      .deleteById(this.entity_name, '@company_id').close()

    // console.log(this.queryMaker);
    return this

  }

}
