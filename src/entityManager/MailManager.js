const EntityManager = require("./entityManager");

module.exports = class CompanyManager extends EntityManager {
  constructor(req, res) {
    super(req, res)
    this.entity_name = 'mail'
  }

  getMailsByApplication() {

    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.query.user_id)
      .saveProps({
        applicant_id: this.req.params.applicant_id,
        advertisment_id: this.req.params.advertisment_id
      }).giveValues()

    // Contrôle de propriétés, sur le current user: il doit être admin, ou candidat

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .setVariables([
        ['@advertisment_id', 'NULL'],
        ['@applicant_id', 'NULL']
      ])
      .selectFieldsOn([
        "@applicant_id :=  applications.applicant_id",
        "@advertisment_id := applications. advertisment_id"
      ], "applications"
      )
      .join([
        ["advertisments", "applications.advertisment_id = advertisments.id"],
        ["companies", "advertisments.company_id = companies.id"]
      ])
      .where([
        [`applications.applicant_id = :applicant_id`, "AND"],
        [`applications.advertisment_id = :advertisment_id`, "AND ("],
        [`applications.applicant_id = :user_id `, 'OR'],
        [`companies.admin_id = :user_id`, ")"]
      ])
      .close()
      .selectFieldsOn(['*'], 'mails').where([
        ["advertisment_id = @advertisment_id", "AND"],
        ["applicant_id = @applicant_id", ""]
      ]).order("mails.date", "DESC")
      .limitAndOffset(
        "10",
        `${this.pool.escape(this.getOffset(this.req.query))}`
      ).close()

    return this
  }

  post() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.body.user_id)
      .saveProps({
        applicant_id: this.req.body.applicant_id,
        advertisment_id: this.req.body.advertisment_id,
        is_from_applicant: this.req.body.is_from_applicant

      })
      .deleteProperties(["applicant_id", "advertisment_id", "is_from_applicant"])
      .addProperties({ date: new Date() }).controlKeys("mail")
      .mapForInsert("mail").giveValues()

    /**
   * nécéssités : 
   *    -vérifier si l'application passée (id= applicant_id+ advertisment_id) est existante, 
   *    comprendre si mail par applicant ou non pour instancier le is_from_applicant
   */


    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .setVariables([
        ['@applicant', 'NULL'],
        ['@advertisment', 'NULL']
      ])
      .onCondition('is_from_applicant',
        (from_applicant) => {
          return from_applicant
            .selectFieldsOn([
              '@applicant := applicant_id',
              '@advertisment := advertisment_id'
            ], 'applications')
            .where([
              ['applications.advertisment_id = :advertisment_id', 'AND'],
              ['applications.applicant_id = :applicant_id', 'AND'],
              ['applications.applicant_id = :user_id', '']
            ])
            .close()
            .setVariables([['@is_from_applicant', 1]])

        },
        (from_admin) => {
          return from_admin
            .selectFieldsOn([
              '@applicant := applicant_id',
              '@advertisment := advertisment_id'
            ], 'applications')
            .join([
              ["advertisments", "applications.advertisment_id = advertisments.id"],
              ["companies", "companies.id = advertisments.company_id"]
            ])
            .where([
              ['applications.advertisment_id = :advertisment_id', 'AND'],
              ['applications.applicant_id = :applicant_id', 'AND'],
              ['companies.admin_id = :user_id', '']

            ]).close()
            .setVariables([['@is_from_applicant', 0]])

        }
      )
      .insert('mail',
        '\`applicant_id\`, \`advertisment_id\`, \`is_from_applicant\`, ',
        '@applicant , @advertisment, @is_from_applicant, '
      ).close()

    return this
  }

  deleteById() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.query.user_id)
      .saveProps({
        mail_id: this.req.params.mail_id,
        is_from_applicant: this.req.params.is_from_applicant,
      }).giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .setVariables([
        ['@mail', 'NULL'],
      ])
      .onCondition('is_from_applicant',
        (from_applicant) => {
          // si l'user di être applicant on controle 
          return from_applicant
            .selectFieldsOn([
              '@mail := mails.id'
            ], 'mails')
            .where([
              ['mails.id = :mail_id', 'AND'],
              ['mails.applicant_id = :user_id', '']
            ])
            .close()

        },
        (from_admin) => {
          return from_admin
            .selectFieldsOn([
              '@mail := mails.id'
            ], 'mails')
            .join([
              ["advertisments", "mails.advertisment_id = advertisments.id"],
              ["companies", "companies.id = advertisments.company_id"]

            ])
            .where([
              ['mails.id = :mail_id', 'AND'],
              ['companies.admin_id = :user_id', '']
            ])
            .close()
        }
      )
      .deleteById('mail', '@mail').close()
    return this


  }


}