const EntityManager = require("./entityManager");
const bcrypt = require('bcrypt')
const jwt = require("jsonwebtoken");

// TODO 1: probleme des espaces dans les titrs d'img et caractere chelous,
// 2: passer une image est obligatoire dans les donnés sinon ça pête (companies_patch) 

module.exports = class UserManager extends EntityManager {
  constructor(req, res) {
    super(req, res)
    this.entity_name = 'user'

    this.tableName = this.entity_name + 's'

    this.authorisedDisplayabledFields = ['users.first_name', 'users.second_name',
      'users.email', 'users.phone_number', 'users.role', 'users.image_url', 'id']
  }

  post() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .parseImgPath(this.req.file, 'profil', 'first_name', 'second_name', 'image_url')
      .addProperties({
        password: bcrypt.hashSync(this.req.body.password, bcrypt.genSaltSync(10))
      })
      .controlKeys('user').mapForInsert('user').giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .insert(this.entity_name).close()


    // super.post()

    return this
  }

  login() {

    this.queryMaker = this.queryMaker
      .init(this.req, {
        email: this.pool.escape(this.req.body.email)
      })
      .selectFieldsOn(['*'], "users")
      .where([
        [`email = :email`, ""]]
      ).close()

    return this


  }
  authentify() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.body.user_id).giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .selectById(this.authorisedDisplayabledFields, this.entity_name).close()

    return this

  }
  patch() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.body.user_id)
    if (this.req.body.password !== undefined) {
      this.values = this.values
        .modifyPropsIfExists('password', bcrypt.hashSync(this.req.body.password, bcrypt.genSaltSync(10)))
    }
    this.values = this.values
      .parseImgPath(this.req.file, 'profil', 'first_name', 'second_name', "image_url")
      .deleteProperties(["role"]).controlKeys("user").mapForUpdate("user")
      .giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .selectById(["image_url"], 'user').close()
      .updateById('user').close()
    return this

  }

  async sendToken(operation) {
    try {
      // est appelé dans deux cas: création user et log
      let row, user;
      if (operation === 'register') {
        row = this.result.query[1]

        user = {
          id: row.insertId,
          role: this.req.body.role
        }
      } else if (operation === 'login') {
        row = this.result.query[1][0]

        const isMatch = await bcrypt.compare(this.req.body.password, row.password)
        if (!isMatch) return res.status(400).send({ error: 'wrong password', code: 32 })
        user = {
          id: row.id,
          role: row.role
        }

      }
      jwt.sign(           // Genere Token puis Repond
        {
          user: user
        }, "c'est bon",
        {
          expiresIn:
            3600 * 24 * 7
        },
        (err, token) => {
          if (err) throw err;
          this.res.status(200).send({ token: token, query: row });
        }
      );
    } catch (e) {
      console.error(e);
      this.res.status(400).send({ error: e });
    }

  }


}