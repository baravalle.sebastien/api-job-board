const queryMaker = require('../utils/makeQuery')
const inputParser = require('../utils/parseForQuery')
const pool = require('../db/mysql')
const doQuery = require('../utils/doQuery')
const respond = require('../utils/respond')
const getOffset = require('../utils/getOffset')
const map_str_search = require('../utils/map_str_search')
const fs = require('fs');

module.exports = class EntityManager {
  constructor(req, res) {
    this.req = req
    this.res = res
    this.pool = pool
    this.queryMaker = queryMaker
    this.inputParser = inputParser
    this.AskToDatabase = doQuery
    this.respond = respond
    this.getOffset = getOffset
    this.mapStrSearch = map_str_search
    this.result = []

    this.authorisedDisplayabledFields = ["*"]
    this.tableName = this.entity_name + 's'
  }
  getAll(fieldsWeWant = this.authorisedDisplayabledFields) {
    this.queryMaker = this.queryMaker.init(this.req)
      .selectFieldsOn(fieldsWeWant, this.tableName)
      .order("id", "DESC")
      .limitAndOffset("10", `${this.pool.escape(this.getOffset(this.req.query))}`)
      .close()
    return this
  }

  getById(fieldsWeWant = this.authorisedDisplayabledFields) {
    const entity = this.entity_name
    let object_to_save = {}
    object_to_save[entity + '_id'] = this.req.params[entity + '_id']
    let values = this.inputParser.instantiate(this.req, this.pool)
      .saveProps(object_to_save).giveValues()

    this.queryMaker = this.queryMaker.init(this.req, values)
      .selectById(fieldsWeWant, entity).close()
    return this
  }
  searchOnStr(column, fieldsWeWant = this.authorisedDisplayabledFields) {

    this.queryMaker = this.queryMaker.init(this.req)
      .selectFieldsOn(fieldsWeWant, this.tableName)
      .where([
        [`${this.mapStrSearch(this.req.query, this.pool, this.entity_name, column)}`, '']
      ])
      .limitAndOffset("10", `${this.pool.escape(this.getOffset(this.req.query))}`)
      .close()
    return this
  }


  deleteById() {
    this.values = this.inputParser.instantiate(this.req.body, this.pool)
      .setUser(this.req.user, this.req.query.user_id).giveValues()

    this.queryMaker = this.queryMaker.init(this.req, this.values)
      .selectById(['image_url'], this.entity_name).close()
      .deleteById(this.entity_name).close()

    return this

  }

  saveImg() {
    // console.log("LOG doit être vide ", this.values.isPath);
    if (typeof (this.values.isPath) === "string") {
      // enregistrer img



      fs.writeFile('./' + this.values.isPath, this.req.file.buffer, function (e) {

      })
    }

    return this

  }
  deleteImg(row, deleteOn = "string") {

    if (row != undefined && typeof (this.values.isPath) === deleteOn && row["image_url"]) {
      try {

        fs.unlinkSync('./' + row["image_url"])
      } catch (e) {
        console.log(e);
      }
    }

    return this

  }

  fetch(default_catch = true) {

    return new Promise((resolve, reject) => {
      let query = this.queryMaker.parseParameters().end()
      this.AskToDatabase(this.pool, query)
        .then((result) => {
          this.result = result
          if (result.result) {  // si la reque

            resolve(this)
          } else {
            reject(this, reject)
          }
        })

    }
    )
      .catch((ManagerWithQueryErr) => {
        if (default_catch === true) {

          ManagerWithQueryErr.sendErr()
        } else {
          throw ManagerWithQueryErr
        }

      })
  }

  send(index = undefined) {
    this.respond(this.res, this.result, index)

  }

  sendErrIf(
    condition, callback
  ) {
    if (condition) {
      this.result.result = false
      this.respond(this.res, this.result, undefined, undefined,
        callback)


    }
  }

  sendErr(
    callBackError = (res, err) => {
      return this.res.status(400).send({ error: err })

    }
  ) {

    this.result.result = false
    this.respond(this.res, this.result, undefined, undefined,
      callBackError)

  }

  testIfEmpty(query) {
    if (!Array.isArray(query) || query.length < 1) {
      return true
    }
    return false

  }




}
