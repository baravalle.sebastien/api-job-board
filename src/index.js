const validator = require('validator')
const express = require('express');


const port = process.env.PORT || 3000
const app = express();
const cors = require('cors')
const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
}
app.use(cors(corsOptions))


const userRouter = require("./routers/users")
const advertismentRouter = require("./routers/advertisments")
const applicationRouter = require("./routers/applications")
const mailsRouter = require("./routers/mails")
const companiesRouter = require("./routers/companies")

// parsing middleware
app.use(express.urlencoded({ extended: true }))
//parse application json
app.use(express.json())
app.use('/img', express.static('img'))
app.use(userRouter)
app.use(advertismentRouter)
app.use(applicationRouter)
app.use(companiesRouter)
app.use(mailsRouter)

app.listen(port, () => {
  console.log('Server is up on port ' + port);
})
