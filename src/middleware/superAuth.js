const jwt = require("jsonwebtoken");
module.exports = function (req, res, next) {
  const token = req.header("x-auth-token");
  if (!token) {
    return res.status(401).json({
      msg: "No token, authorization denied",
    });
  }

  try {
    const decoded = jwt.verify(token, "c'est bon");
    req.user = decoded.user;
    if (req.user.role !== 2) throw Error("")
    next();
  } catch (err) {
    res.status(401).json({ code: 3, message: "Token is not valid" });
  }
};
