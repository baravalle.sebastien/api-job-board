const express = require('express')

const router = new express.Router()


const multer = require('multer')
const upload = multer(
  {
    fileSize: 1000000
  }

)
const auth = require('../middleware/auth');

let advertismentManager = require('../entityManager/AdvertismentManager')

router.get('/advertisments', async (req, res) => {
  const AdvertismentManager = new advertismentManager(req, res)
  AdvertismentManager.getAll()
    .fetch().then((MRow) => {
      MRow.send(1)
    })

})

router.get('/advertisment/:advertisment_id', async (req, res) => {

  const AdvertismentManager = new advertismentManager(req, res)
  AdvertismentManager.getById()
    .fetch().then((MRow) => {
      MRow.send(1)
    })
})

router.get('/advertismentbycompany/:company_id', async (req, res) => {

  const AdvertismentManager = new advertismentManager(req, res)
  AdvertismentManager.getByCompany()
    .fetch().then((MRow) => {
      MRow.send(1)
    })
})
router.get('/myadvertisment', auth, async (req, res) => {

  const AdvertismentManager = new advertismentManager(req, res)
  AdvertismentManager.getMy()
    .fetch().then((MRow) => {
      MRow.send(1)
    })
})

// recherche par début de phrase
router.get('/search_advertisment_str',
  (req, res) => {
    const AdvertismentManager = new advertismentManager(req, res)
    AdvertismentManager.searchOnStr(['companies.name', 'title'])
      .fetch().then((MRow) => {
        MRow.send(1)
      })
  })

//  TODO : rajouter un express-validator ici? 

router.post('/advertisment', auth, upload.single("img_company"),
  async function (req, res) {

    const AdvertismentManager = new advertismentManager(req, res)
    AdvertismentManager.post()
      .fetch().then((MRow) => {
        MRow.saveImg()
          .send(3)
      })

  }
)

router.patch('/advertisment/:advertisment_id', auth,
  (req, res) => {
    const AdvertismentManager = new advertismentManager(req, res)
    AdvertismentManager.patch()
      .fetch().then((MRow) => {
        MRow.send()
      })
  }
)

router.delete('/advertisment/:advertisment_id', auth,
  async (req, res) => {
    /**
     * DELETE précisément :
     * si on delete une propriété, verify property
     * delete by id, -> select image path
     * delete img
     */
    const AdvertismentManager = new advertismentManager(req, res)
    AdvertismentManager.deleteById().fetch()
      .then((MRow) => {
        MRow.send()
      })


  }
)

module.exports = router