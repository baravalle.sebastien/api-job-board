const express = require('express')
const router = new express.Router()

const auth = require('../middleware/auth');

const { check } = require("express-validator");

const checkInputs = require('../utils/checkInputs')

let applicationManager = require('../entityManager/ApplicationManager')

router.get('/applications', auth, (req, res) => {

	const ApplicationManager = new applicationManager(req, res)
	ApplicationManager.getAll()
		.fetch().then((MRow) => {
			MRow.send(1)
		})

})
router.get('/applicationsbyadvertisment/:advertisment_id', auth, (req, res) => {

	const ApplicationManager = new applicationManager(req, res)
	ApplicationManager.getByAdvertisment()
		.fetch().then((MRow) => {
			MRow.send(3)
		})

})

router.post('/application', [
	check("advertisment_id", "advertisment_id is required").not().isEmpty()

],
	auth,
	async function (req, res) {

		if (!checkInputs(req, res)) return false;
		const ApplicationManager = new applicationManager(req, res)
		ApplicationManager.post()
			.fetch().then((MRow) => {
				MRow.send(4)
			})
	}
)

router.delete('/application/:advertisment_id', auth,
	async (req, res) => {
		/**
		 * Seul applicant peut suprimer applications, 
		 * mais si aadmin suprime son advertisment-> by cascade applications supprimés 
		 * DELETE précisément :
		 * si on delete une propriété, verify property
		 * delete by id, -> select image path
		 * delete img
		 */
		const ApplicationManager = new applicationManager(req, res)
		ApplicationManager.deleteByadvertisment_andCurrentUser()
			.fetch()
			.then((MRow) => {
				MRow.send()
			})


	}
)




module.exports = router