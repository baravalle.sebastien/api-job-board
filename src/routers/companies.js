const express = require('express')

const router = new express.Router()
const multer = require('multer')
const upload = multer(
  {
    fileSize: 1000000
  }
)

const auth = require('../middleware/auth')
const { check } = require("express-validator")

const checkInputs = require('../utils/checkInputs')

//  TODO: rajouter un controle de role en lus du ctrl de propriété

let companyManager = require('../entityManager/CompanyManager')

router.get('/companies', async function (req, res) {

  const CompanyManager = new companyManager(req, res)
  CompanyManager.getAll()
    .fetch().then((MRow) => {
      MRow.send(1)
    })

})

router.get('/company/:company_id', function (req, res) {

  const CompanyManager = new companyManager(req, res)
  CompanyManager.getById()
    .fetch().then((MRow) => {
      MRow.send(2)
    })
})

router.get('/mycompanies/', auth, async function (req, res) {

  const CompanyManager = new companyManager(req, res)
  CompanyManager.getMyCompanies()
    .fetch().then((MRow) => {
      MRow.send(1)
    })

})

router.post('/company', auth, upload.single("img_company"),
  [
    check("companies.name", "companies.name is required").not().isEmpty()
  ]
  ,
  async function (req, res) {
    if (!checkInputs(req, res)) return false;

    const CompanyManager = new companyManager(req, res)
    CompanyManager.post()
      .fetch().then((MRow) => {
        MRow.saveImg()
          .send()
      })

  })

router.patch('/company/:company_id', auth, upload.single("img_company"),
  async function (req, res) {
    const CompanyManager = new companyManager(req, res)
    CompanyManager.patch()
      .fetch().then((MRow) => {
        MRow.deleteImg(MRow.result.query[4][0])
          .saveImg()
          .send()
      })
  }
)

router.delete('/company/:company_id', auth,
  async (req, res) => {

    const CompanyManager = new companyManager(req, res)
    CompanyManager.deleteById().fetch()
      .then((MRow) => {
        MRow.deleteImg(MRow.result.query[4][0])
          .send()

      })


  }
)



module.exports = router