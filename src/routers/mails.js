const express = require('express')
const pool = require('../db/mysql')
const router = new express.Router()
const parseForQuery = require('../utils/parseForQuery')
const auth = require('../middleware/auth');
const doQuery = require('../utils/doQuery')
const { check } = require("express-validator");
const makeQuery = require('../utils/makeQuery');
const getOffset = require('../utils/getOffset');
const checkInputs = require('../utils/checkInputs')
const respond = require('../utils/respond')
let mailManager = require('../entityManager/MailManager')

router.get('/conversation/:advertisment_id/:applicant_id',
  auth,
  (req, res) => {

    const MailManager = new mailManager(req, res)
    MailManager.getMailsByApplication()
      .fetch().then((MRow) => {
        MRow.send(4)
      })

  }
)

// Add a mails to Applications
router.post('/mail',
  [
    check("advertisment_id", "advertisment_id is required").not().isEmpty(),
    check("is_from_applicant", "is_from_applicant is required").not().isEmpty(),
    check("applicant_id", "applicant_id is required").not().isEmpty(),
    check("content", "content is required").not().isEmpty(),
  ]
  , auth, async (req, res) => {

    if (!checkInputs(req, res)) return false;

    const MailManager = new mailManager(req, res)
    MailManager.post()
      .fetch().then((MRow) => {
        MRow.send(5)
      })

  })

router.delete('/mail/:mail_id/:is_from_applicant', auth,
  async (req, res) => {
    const MailManager = new mailManager(req, res)
    MailManager.deleteById().fetch()
      .then((MRow) => {
        MRow.send()
      })
  }
)

module.exports = router