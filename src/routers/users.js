const express = require('express')

const router = new express.Router()
const multer = require('multer')
const { check } = require("express-validator");
const checkInputs = require('../utils/checkInputs')

const auth = require('../middleware/auth');
const superAuth = require('../middleware/superAuth');
const upload = multer(
  {
    fileSize: 100000000
  }
)

let userManager = require('../entityManager/UserManager')


router.get('/users', superAuth,
  (req, res) => {
    const UserManager = new userManager(req, res)
    UserManager.getAll()
      .fetch().then((MRow) => {
        MRow.send(1)
      })

  })
router.get('/user/:user_id',
  (req, res) => {

    let UserManager = new userManager(req, res)
    UserManager.getById()
      .fetch().then((MRow) => {
        MRow.send(2)
      })

  }
)

// recherche par début de phrase
router.get('/search_user_str?',
  (req, res) => {

    let UserManager = new userManager(req, res)

    UserManager.searchOnStr(['email'])
      .fetch().then((MRow) => {
        MRow.send()
      })

  }
)


router.post('/register', upload.single("img_profil"),

  [
    check("first_name", "first_name is required").not().isEmpty(),
    check("second_name", "second_name is required").not().isEmpty(),
    check("email", "email is required").isEmail().not().isEmpty(),
    check("role", "role is required").not().isEmpty(),
    check("phone_number", "phone_number is required").not().isEmpty(),
    check("password", "password is required").not().isEmpty()
  ],
  async (req, res) => {
    if (!checkInputs(req, res)) return false;
    let UserManager = new userManager(req, res)
    UserManager.post()
      .fetch()
      .then((MRow) => {
        MRow.saveImg().sendToken('register')
      })

  })

router.post("/auth", [
  check("email", "Email is required").isEmail(),
  check("password", "Password is required").exists()

], async (req, res) => {

  if (!checkInputs(req, res)) return false;

  let UserManager = new userManager(req, res)

  UserManager.login()
    .fetch().then((MRow) => {

      MRow.sendErrIf(
        MRow.testIfEmpty(MRow.result.query[1]),
        (res, err) => {     // On error
          return res.status(400).send({ error: 'email does not exists', code: 31, err: err })
        }
      )

      MRow.sendToken('login')
    })
})

router.get("/auth", auth, async (req, res) => {

  let UserManager = new userManager(req, res)

  UserManager.authentify().fetch().then((MRow) => {
    MRow.send(2)
  })

});

router.patch('/user', auth, upload.single("img_profil"),
  async (req, res) => {

    let UserManager = new userManager(req, res)
    UserManager.patch()
      .fetch()
      .then((MRow) => {
        MRow.deleteImg(MRow.result.query[2][0])
          .saveImg().send(3)
      })
  }
)

router.delete('/user', auth,
  async (req, res) => {
    /**
     * DELETE précisément :
     * si on delete une propriété, verify property
     * delete by id, -> select image path
     * delete img
     */

    const UserManager = new userManager(req, res)
    UserManager.deleteById().fetch()
      .then((MRow) => {

        MRow.deleteImg(MRow.result.query[2][0], "undefined").send(3)
      })


  }
)

module.exports = router