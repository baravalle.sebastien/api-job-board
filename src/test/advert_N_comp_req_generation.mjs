import faker from 'faker';

import fetch from 'node-fetch';
import cheerio from 'cheerio'


const toDataURL = (url) => {

  return fetch(url)
    .then(res => {
      console.log("URL = ", url);
      var mimetype = res.headers.get('Content-Type')
      return {
        mimetype,
        buf: res.buffer()
      }
    })

}
const wikiarticle = function (word, lang) {

  const url = `https://${lang || "en"}.wikipedia.org/wiki/${word || "Special:random"}`

  return new Promise(resolve => {
    fetch(url)
      .then(res => {
        res.text().then(text => {


          var $ = cheerio.load(text);
          // let title = $('h1').text()
          let result = $('#mw-content-text .mw-parser-output:first-of-type  p').text()
          resolve(result)
        })
      })

  })

};

export default async function (reqUser) {

  return new Promise((resolve) => {

    const deter_img_type = faker.datatype.number({
      'min': 1,
      'max': 6
    });
    const num = faker.datatype.number({
      'min': 1,
      'max': 13
    });
    const num2 = faker.datatype.number({
      'min': 0,
      'max': 9
    });
    const num3 = faker.datatype.number({
      'min': 0,
      'max': 5
    });
    const url = deter_img_type === 1 ? 'https://d2n9ha3hrkss16.cloudfront.net/uploads/stage/stage_image/771' + num2 + '' + num3 + '/optimized_large_thumb_stage.jpg'
      : deter_img_type === 2 ? 'https://raw.githubusercontent.com/pigment/fake-logos/gh-pages/logos/medium/color/' + num + '.png'
        : deter_img_type === 3 ? 'https://d2n9ha3hrkss16.cloudfront.net/uploads/stage/stage_image/624' + num2 + '' + num3 + '/optimized_large_thumb_stage.jpg' :
          deter_img_type === 4 ?
            'https://d2n9ha3hrkss16.cloudfront.net/uploads/stage/stage_image/597' + num2 + '' + num3 + '/optimized_large_thumb_stage.jpg' :
            deter_img_type === 5 ?
              'https://d2n9ha3hrkss16.cloudfront.net/uploads/stage/stage_image/642' + num2 + '' + num3 + '/optimized_large_thumb_stage.jpg' :
              'https://d2n9ha3hrkss16.cloudfront.net/uploads/stage/stage_image/1140' + num2 + '' + num3 + '/optimized_large_thumb_stage.jpg'


    toDataURL(url)
      .then(buf => {
        buf.buf.then(buffer => {

          // fais de beaux email..
          const deter_company_type = faker.datatype.number({
            'min': 1,
            'max': 3
          });
          const deter_phone_type = faker.datatype.number({
            'min': 1,
            'max': 3
          });


          const funny_comp_name = faker.company.catchPhraseDescriptor() + ' ' + faker.commerce.productMaterial() + faker.animal.horse()
          const funny_comp_name2 = (faker.animal.type() + ' ' + faker.company.catchPhraseNoun()).toLocaleUpperCase()

          const company_name = deter_company_type === 1 ? faker.company.companyName() :
            deter_company_type === 2 ? funny_comp_name
              : funny_comp_name2

          let title = faker.name.jobTitle()
          let num_adress = faker.datatype.number({
            'min': 0,
            'max': 112
          });
          let wages = faker.datatype.float({
            'min': 0,
            'max': 10000
          });
          let working_time = faker.datatype.number({
            'min': 1,
            'max': 65
          });
          let start_date = faker.date.soon()
          // start_date = start_date.slice(0, 10) + ' ' + start_date.slice(11, 19)
          let req = {
            body: {
              "companies.name": company_name,
              "companies.description": faker.lorem.paragraph(),
              title: title,
              working_place:
                num_adress + ' ' + faker.address.streetName() + ' ' + faker.address.city(),
              full_description: "",
              excerpt: "",
              wages
              , start_date, working_time,

            },
            file: {
              fieldname: 'img_profil',
              originalname: 'texture4.jpg',
              encoding: '7bit',
              mimetype: buf.mimetype,
              buffer: buffer,
              size: Buffer.byteLength(buffer)

            },
            user: reqUser
          }


          let prof = title.split(" ")
          wikiarticle(prof[2], "en").then(res => {
            req.body.full_description = res
            req.body.excerpt = res.slice(0, 250)
            resolve(req)
          })

        })
      })
  })
}

