import faker from 'faker';

import fetch from 'node-fetch';
import cheerio from 'cheerio'


const wikiarticle = function (word, lang) {

  const url = `https://${lang || "en"}.wikipedia.org/wiki/${word || "Special:random"}`

  return new Promise(resolve => {
    fetch(url)
      .then(res => {
        res.text().then(text => {


          var $ = cheerio.load(text);
          // let title = $('h1').text()
          let result = $('#mw-content-text .mw-parser-output:first-of-type  p').text()
          resolve(result)
        })
      })

  })

};

export default async function (reqUser, comp_id) {

  return new Promise((resolve) => {



    // fais de beaux email..
    const deter_company_type = faker.datatype.number({
      'min': 1,
      'max': 3
    });
    const deter_phone_type = faker.datatype.number({
      'min': 1,
      'max': 3
    });


    const funny_comp_name = faker.company.catchPhraseDescriptor() + ' ' + faker.commerce.productMaterial() + faker.animal.horse()
    const funny_comp_name2 = (faker.animal.type() + ' ' + faker.company.catchPhraseNoun()).toLocaleUpperCase()

    const company_name = deter_company_type === 1 ? faker.company.companyName() :
      deter_company_type === 2 ? funny_comp_name
        : funny_comp_name2

    let title = faker.name.jobTitle()
    let num_adress = faker.datatype.number({
      'min': 0,
      'max': 112
    });
    let wages = faker.datatype.float({
      'min': 0,
      'max': 10000
    });
    let working_time = faker.datatype.number({
      'min': 1,
      'max': 65
    });
    let start_date = faker.date.soon()
    // start_date = start_date.slice(0, 10) + ' ' + start_date.slice(11, 19)
    let req = {
      body: {
        title: title,
        working_place:
          num_adress + ' ' + faker.address.streetName() + ' ' + faker.address.city(),
        full_description: "",
        excerpt: "",
        wages
        , start_date, working_time,

      },
      user: reqUser
    }


    let prof = title.split(" ")
    wikiarticle(prof[2], "en").then(res => {
      req.body.full_description = res
      req.body.excerpt = res.slice(0, 250)
      resolve(req)
    })

  })
}
