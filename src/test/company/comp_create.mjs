import companyManager from '../../entityManager/CompanyManager'
import fake_res from '../fake_res'
import fetch from 'node-fetch';
import fs from 'fs'

const comp_create = async function (req) {
  return new Promise((resolve, reject) => {

    const CompanyManager = new companyManager(req, fake_res)
    CompanyManager.post()
      .fetch().then(
        (MRow) => {
          try {
            console.log("company  enregisté");

            if (MRow && MRow.values !== undefined) {

              fs.writeFile('../' + MRow.values.isPath, MRow.req.file.buffer, function () {
                console.log("company :", MRow.values.isPath);
                resolve({
                  req: req,
                  query: MRow.result.query
                })
              })
            } else {
              reject()

            }

          } catch (e) {
            console.log(e);
            reject()
          }
        }

      )
  })
}

export default comp_create

