import comp_req_generation from "./comp_req_generation .mjs";
import comp_create from "./comp_create.mjs";
const fillCompanies = async function (array_admin) {
  return new Promise(async resolve => {
    let array_comp = []

    for (let i = 0; i < array_admin.length; i++) {
      let req = await comp_req_generation({ id: array_admin[i], role: 1 })
      req = await comp_create(req)
      if (req != undefined) {

        const comp_id = req.query[3].insertId
        console.log("ID de company", comp_id);
        array_comp.push(comp_id)
      }

    }

    resolve(array_comp)
  })
}
export default fillCompanies