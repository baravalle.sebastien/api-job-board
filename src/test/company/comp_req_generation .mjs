import faker from 'faker';

import fetch from 'node-fetch';
import cheerio from 'cheerio'


const toDataURL = (url) => {

  return new Promise(resolve => {

    fetch(url)
      .then(res => {
        console.log("URL = ", url);
        var mimetype = res.headers.get('Content-Type')
        res.buffer().then(buf => {
          resolve({
            mimetype, buf
          })
        })
      })
  })

}
const wtj_crawl = function () {

  let page = Math.floor(Math.random() * 999)

  let li = Math.floor(Math.random() * 30)

  // const url = `https://www.welcometothejungle.com/fr/companies?page=${page}&aroundQuery=France&refinementList%5Boffices.country_code%5D%5B%5D=FR`
  // console.log(url);
  const url = "https://csekhvms53-3.algolianet.com/1/indexes/*/queries?x-algolia-agent=Algolia%30for%30JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20JS%20Helper%20(3.5.5)%3B%20react%20(17.0.2)%3B%20react-instantsearch%30(6.12.1)&x-algolia-application-id=CSEKHVMS53&x-algolia-api-key=02f0d440abc99cae37e126886438b266"

  let headers = {

    'Host': 'csekhvms53-3.algolianet.com',
    'Connection': 'keep-alive',
    'Content-Length': '1806',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache',
    'sec-ch-ua': '";Not A Brand";v="99", "Chromium";v="94"',
    'accept': 'application/json',
    'content-type': 'application/x-www-form-urlencoded',
    'sec-ch-ua-mobile': '?0',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
    'sec-ch-ua-platform': '"Linux"',
    'Origin': 'https://www.welcometothejungle.com',
    'Sec-Fetch-Site': 'cross-site',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Dest': 'empty',
    'Referer': 'https://www.welcometothejungle.com/',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7',
  }
  let body = `{ "requests": [{ "indexName": "wk_cms_organizations_production", "params": "highlightPreTag=%3Cais-highlight-0000000000%3E&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&filters=website.reference%3Awttj_fr&restrictSearchableAttributes=%5B%22name%22%2C%22alternative_names%22%2C%22offices.city%22%2C%22offices.state%22%2C%22offices.country%22%2C%22sectors.name.fr%22%2C%22sectors.parent.fr%22%2C%22tools_name%22%5D&hitsPerPage=1&query=&maxValuesPerFacet=100&page=${page}&facets=%5B%22offices.country_code%22%…re%22%2C%22sectors_name.fr.Association%20%2F%20ONG%22%2C%22sectors_name.fr.…Mode%20%2F%20Luxe%20%2F%20Beaut%C3%A9%20%2F%20Art%20de%20vivre%22%2C%22sectors_name.fr.Publicit%C3%A9%20%2F%20Marketing%20%2F%20Agence%22%2C%22sectors_name.fr.Sant%C3%A9%20%2F%20Social%20%2F%20Environnement%22%2C%22sectors_name.fr.Secteur%20public%20et%20administration%22%2C%22sectors_name.fr.Services%20aux%20entreprises%22%2C%22sectors_name.fr.Tech%22%5D&tagFilters=&facetFilters=%5B%22offices.country_code%3AFR%22%5D" }] }`
  let init = {
    method: 'POST',
    headers: headers,
    mode: 'strict-origin-when-cross-origin',
    body: body
  }

  return new Promise(resolve => {
    fetch(url, init)
      .then(res => {

        res.json().then(text => {
          const hit = text.results[0].hits[0]
          const url_img = hit.logo.url
          const name = hit.name
          const pres_url = 'https://www.welcometothejungle.com/fr/companies/' + hit.slug;

          const siren_url = 'https://www.societe.com/cgi-bin/search?champs=' + hit.slug;

          console.log('url_img', url_img);
          console.log('name', name);
          console.log('pres_url', pres_url);
          console.log(hit.sectors_name.fr);
          resolve({
            url_img,
            name,
            pres_url,
            siren_url
          })
        })


      })

  })

};

const get_pres = function (url) {


  return new Promise(resolve => {
    fetch(url)
      .then(res => {
        res.text().then(text => {

          var $ = cheerio.load(text);
          let pres = $('article.jhUYis div.bLmqMr.GZHDv').text()
          resolve(pres)
        })
      })

  })

};
let headers = {
  'Host': 'www.societe.com',
  'Connection': 'keep-alive',
  'Pragma': 'no-cache',
  'Cache-Control': 'no-cache',
  'sec-ch-ua': '";Not A Brand";v="99", "Chromium";v="94"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"Linux"',
  'Upgrade-Insecure-Requests': '1',
  'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
  'Sec-Fetch-Site': 'none',
  'Sec-Fetch-Mode': 'navigate',
  'Sec-Fetch-User': '?1',
  'Sec-Fetch-Dest': 'document',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7',
  'Cookie': 'dbps=12; didomi_token=eyJ1c2VyX2lkIjoiMTdiYjRlNWItMTRmMS02YzA0LThkMmItYmY2Yjc0ZGQ1OWEzIiwiY3JlYXRlZCI6IjIwMjEtMDktMDVUMDc6Mzk6NDYuNTQwWiIsInVwZGF0ZWQiOiIyMDIxLTA5LTA1VDA3OjM5OjQ2LjU0MFoiLCJ2ZW5kb3JzIjp7ImVuYWJsZWQiOlsiZ29vZ2xlIiwiYzpnb29nbGVhbmEtNFRYbkppZ1IiXX0sInB1cnBvc2VzIjp7ImVuYWJsZWQiOlsiZGV2aWNlX2NoYXJhY3RlcmlzdGljcyIsImdlb2xvY2F0aW9uX2RhdGEiXX0sInZlbmRvcnNfbGkiOnsiZW5hYmxlZCI6WyJnb29nbGUiXX0sInZlcnNpb24iOjIsImFjIjoiRExVQUFBQUFBQklCQkNJQUVFUUFDQVFvRVlHVUFTQVFBSUNJSUJBZ0FnRkFCQUFnZ0FJUUFBRUFNd0FnQUFBQkVBQkVKQUVGQUlJSWhJUUFBQUFpRXdSQUFEQUFDQUlCZ0pLQVVBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUJFQUFBQUFBQUJBQU1BQk1CQUVBQkFBQUlBQUFFSUFBSUJBZ0FBQUFBQlJBQUJBQUFKRmdnaFFBRUFBRUNWQVFBQUVCQVFTQUlnQUFLSUJNRVFBeUFBQUlBQW9JQ2lBRUVBUUFBQUJBQ0F3QkFBQUFFQkdBQUFDZ1FZQkFBQUFCQUFDSUJBQUlDQklJSUJHQkFCQUFnQUFDQUJRQVVRWWdBRkVBZ0dRSUFFSUFCRUFDSUFDQW9BQlFoclFBRUNJQ2dFQXdBQVFBUUFLQUFEbkFJQUFDRVlBQUVCRURDSUNLb0FDZ0FSUVFDQ1JJVWdnQ0FGRWdBQVFFbWtBZ0FBUUFnQUVnQ0FCRWlBU1R3UUFuQWlEQUVLQ1ZFcUJRQUlFQUZnaUVDb3gwZ3dRREN3S0FnQm5KUVlRWURPb0VER2czT093R1JBc3hJQkJnQUVBUUFBQUJ1QUNSZ2tRTXhnTXJpbnFMVkh2XzEyd003N2wwREJZSWZxY2RjWFMwNlBEQUpoeTlNV3I2QUdkWXJZX0xvWkJITVRFTUNfNG9BSE5UZTc3TU5BQUEuRExVQUFBQUFBQklCQkNJQUVBUUFDQVFvRVlHVUFTQVFBSUNJSUJBZ0FnRkFCQUFnZ0FJUUFBRUFNd0FnQUFBQkVBQkVKQUVGQUlJSWhJUUFBQUFpRXdSQUFEQUFDQUlCZ0pLQVVBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUJFQUFBQUFBQUJBQU1BQk1CQUVBQkFBQUlBQUFFSUFBSUJBZ0FBQUFBQlJBQUJBQUFKRmdnaFFBRUFBRUNWQVFBQUVCQVFTQUlnQUFLSUJNRVFBeUFBQUlBQW9JQ2lBRUVBUUFBQUJBQ0F3QkFBQUFFQkdBQUFDZ1FZQkFBQUFCQUFDSUJBQUlDQklJSUJHQkFCQUFnQUFDQUJRQVVRWWdBRkVBZ0dRSUFFSUFCRUFDSUFDQW9BQlFoclFBRUNJQ2dFQXdBQVFBUUFLQUFEbkFJQUFDRVlBQUVCRURDSUNLb0FDZ0FSUVFDQ1JJVWdnQ0FGRWdBQVFFbWtBZ0FBUUFnQUVnQ0FCRWlBU1R3UUFuQWlEQUVLQ1ZFcUJRQUlFQUZnaUVDb3gwZ3dRREN3S0FnQm5KUVlRWURPb0VER2czT093R1JBc3hJQkJnQUVBUUFBQUJ1QUNSZ2tRTXhnTXJpbnFMVkh2XzEyd003N2wwREJZSWZxY2RjWFMwNlBEQUpoeTlNV3I2QUdkWXJZX0xvWkJITVRFTUNfNG9BSE5UZTc3TU5BQUEifQ==; euconsent-v2=CPMDIqZPMDIqZAHABBENBpCsAP_AAH_AAAAAILtf_X__b3_j-_59f_t0eY1P9_7_v-0zjhfdt-8N2f_X_L8X42M7vF36pq4KuR4Eu3LBIQdlHOHcTUmw6okVrzPsbk2Mr7NKJ7PEmnMbO2dYGH9_n93TuZKY7__8___z__-v_v____f_r-3_3__5_X---_e_V399zLv9____39nN___9uCCYBJhqXkAXYljgybRpVCiBGFYSHQCgAooBhaJrCBlcFOyuAj1BCwAQmoCMCIEGIKMGAQACAQBIREBIAeCARAEQCAAEAKkBCAAjYBBYAWBgEAAoBoWIEUAQgSEGRwVHKYEBEi0UE9lYAlF3saYQhllgBQKP6KjARKEECwMhIWDmOAJAS4AA.f_gAD_gAAAAA; __aaxsc=1; aasd=10%7C1634473395735'
}
var init = {
  method: 'GET',
  headers: headers,
  mode: 'strict-origin-when-cross-origin'
}
const get_siren = function (url) {


  console.log("SIRENurl = ", url);
  return new Promise(resolve => {
    fetch(url, init)
      .then(res => {
        res.text().then(text => {

          var $ = cheerio.load(text);
          let link_to_society = $('#search div.Card.frame > a:first-of-type').attr('href')
          // .data("href")
          // .data("href")
          link_to_society = 'https://www.societe.com' + link_to_society
          console.log("link to so", link_to_society);
          const siren = link_to_society.replace(/\D/g, "")
          resolve({ siren, link_to_society })
        })
      })

  })

};
const get_naf = function (url) {

  return new Promise(resolve => {
    fetch(url, init)
      .then(res => {
        res.text().then(text => {

          var $ = cheerio.load(text);
          let list_script = $('#ape-histo-description').text()
          const match_naf = list_script.match(/\((.*)\)/);
          resolve(match_naf[1])
        })
      })

  })

};




const comp_req_generation = async function (reqUser = { id: 10, role: 1 }) {
  const company_params = await wtj_crawl()
  const pres = await get_pres(company_params.pres_url)
  const img = await toDataURL(company_params.url_img)
  const siren = await get_siren(company_params.siren_url)
  console.log("siren", siren.siren);
  const naf = await get_naf(siren.link_to_society)


  const req = {
    body: {
      "companies.name": company_params.name,
      "companies.description": pres,
      "companies.siren": siren.siren,
      "companies.naf": naf


    },
    file: {
      fieldname: 'img_profil',
      originalname: 'texture4.jpg',
      encoding: '7bit',
      mimetype: img.mimetype,
      buffer: img.buf,
      size: Buffer.byteLength(img.buf)

    },
    user: reqUser
  }

  return req;
}

export default comp_req_generation
