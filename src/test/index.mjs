import fillUsers from "./user/user_fill.mjs"
import fillCompanies from "./company/comp_fill"

const fillDB = async function () {
  try {

    const arrays = await fillUsers()
    const comp_id = await fillCompanies(arrays.admin_array)
    console.log("Arrays de comp = ", comp_id);
  } catch (e) {
    console.log(e);

  }



}
fillDB()