import faker from 'faker';
import fetch from 'node-fetch';
import fs from 'fs'


import userManager from '../entityManager/UserManager';
import advertismentManager from '../entityManager/AdvertismentManager'
import user_req_generation from './user/user_req_generation.mjs';
let arrayOfUsersReq = []

import advertisment_req_generation from './advertisment_req_generation.mjs';

let res = {
  status: function (e) {
    console.log("STATUS=>", e);
    return this
  },
  send: function (e) {
    console.log('send :', e);
    return this
  }
}

let instantiateUser = async function (req, res) {
  return new Promise((resolve, reject) => {

    const UserManager = new userManager(req, res)
    UserManager.post()
      .fetch().then(
        (MRow) => {
          try {
            req.user = {
              id: MRow.result.query[1].insertId,
              role: req.body.role
            }

            console.log("user " + req.user.id + " enregisté");


            fs.writeFile('../' + MRow.values.isPath, MRow.req.file.buffer, function () {
              console.log("photo profil :", MRow.values.isPath);
              resolve(req)
            })

          } catch (e) {
            console.log(e);
            reject()
          }
        }

      )
  })
}
let instantiateCompanyAndAdvert = async function (req, res) {
  return new Promise((resolve, reject) => {

    const AdvertismentManager = new advertismentManager(req, res)
    AdvertismentManager.post()
      .fetch().then(
        (MRow) => {
          try {
            console.log("company  enregisté");

            fs.writeFile('../' + MRow.values.isPath, MRow.req.file.buffer, function () {
              console.log("company :", MRow.values.isPath);
              resolve({
                req: req,
                query: MRow.result.query
              })
            })

          } catch (e) {
            console.log(e);
            reject()
          }
        }

      )
  })
}


let test = async function () {
  return new Promise(async resolve => {

    for (let i = 0; i < 1; i++) {
      let req = await user_req_generation(arrayOfUsersReq)
      console.log("ITERATION n ", i);
      req = await instantiateUser(req, res)
      if (req.body.role === 1) {
        const admin_id = req.user.id
        req = await advertisment_req_generation(req.user)
        // console.log(req);

        req = await instantiateCompanyAndAdvert(req, res)
        console.log('return de create comp and advert', req.query);
        const comp_id = req.query[3].insertId
        const advertisment_id = req.query[5].insertId
        console.log('comp = ', comp_id, ' Id = ', advertisment_id);

      } else {
        const applicant_id = req.user.id

      }
    }
    resolve()
  })
}

let create = async function () {
  return new Promise(async resolve => {

    for (let i = 0; i < 1; i++) {
      console.log("ITERATION n ", i);
      let req = await user_req_generation(arrayOfUsersReq)
      req = await instantiateUser(req, res)
      if (req.body.role === 1) {
        const admin_id = req.user.id
        req = await advertisment_req_generation(req.user)
        // console.log(req);

        req = await instantiateCompanyAndAdvert(req, res)
        console.log('return de create comp and advert', req.query);
        const comp_id = req.query[3].insertId
        const advertisment_id = req.query[5].insertId
        console.log('comp = ', comp_id, ' Id = ', advertisment_id);

      } else {
        const applicant_id = req.user.id

      }
    }
    resolve()
  })
}


