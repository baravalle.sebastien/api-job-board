import fake_res from '../fake_res'

import userManager from '../../entityManager/UserManager';
import fetch from 'node-fetch';
import fs from 'fs'

let instantiateUser = async function (req) {
  return new Promise((resolve, reject) => {

    const UserManager = new userManager(req, fake_res)
    UserManager.post()
      .fetch().then(
        (MRow) => {
          try {
            req.user = {
              id: MRow.result.query[1].insertId,
              role: req.body.role
            }

            console.log("user " + req.user.id + " enregisté");


            fs.writeFile('../' + MRow.values.isPath, MRow.req.file.buffer, function () {
              console.log("photo profil :", MRow.values.isPath);
              resolve(req)
            })

          } catch (e) {
            console.log(e);
            reject()
          }
        }

      )
  })
}
export default instantiateUser