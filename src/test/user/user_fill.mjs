import user_create from "./user_create"
import user_req_generation from "./user_req_generation.mjs"

const fillUsers = async function () {
  return new Promise(async resolve => {
    let admin_array = []
    let applicant_array = []
    for (let i = 0; i < 10; i++) {
      let req = await user_req_generation()
      req = await user_create(req)

      if (req.body.role === 1) {
        const admin_id = req.user.id
        admin_array.push(admin_id)

      } else {
        const applicant_id = req.user.id
        applicant_array.push(applicant_id)

      }
    }
    resolve({
      admin_array,
      applicant_array
    })
  })
}

export default fillUsers