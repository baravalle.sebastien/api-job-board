import faker from 'faker';
import fetch from 'node-fetch';

const toDataURL = (url) => {

  return fetch(url)
    .then(res => {
      var mimetype = res.headers.get('Content-Type')
      return {
        mimetype,
        buf: res.buffer()
      }
    })

}

const user_req_generation = async function () {



  return new Promise((resolve) => {

    const deter_img_type = faker.datatype.number({
      'min': 1,
      'max': 4
    });
    const url = deter_img_type === 1 ? faker.internet.avatar()
      : deter_img_type === 2 ? 'https://thispersondoesnotexist.com/'
        : deter_img_type === 3 ? 'https://picsum.photos/200/300' : 'https://i.pravatar.cc/300'

    toDataURL(url)
      .then(buf => {
        buf.buf.then(buffer => {
          // fs.writeFileSync('./test' + i + '.jpeg', buffer)
          const first_name = faker.name.firstName()
          const second_name = faker.name.lastName()

          // fais de beaux email..
          const deter_email_type = faker.datatype.number({
            'min': 1,
            'max': 3
          });
          const deter_phone_type = faker.datatype.number({
            'min': 1,
            'max': 3
          });
          let deter_role_type = faker.datatype.number({
            'min': 0,
            'max': 1
          }) * faker.datatype.number({
            'min': 0,
            'max': 1
          });

          const domain = faker.internet.email().split('@')[1]
          const re = new RegExp(/[^a-z]/, 'gi')


          const funny_email = (faker.animal.cetacean() + faker.vehicle.vehicle()).toLowerCase().replace(re, '_')
          const email = deter_email_type === 1 ? faker.internet.email() :
            deter_email_type === 2 ? first_name + '.' + second_name + '@' + domain
              : funny_email + '@' + domain


          let req = {
            body: {
              first_name: first_name,
              second_name: second_name,
              email: email,
              phone_number: '0660609543',
              role: 1,
              password: "azerty"

            },
            file: {

              fieldname: 'img_profil',
              originalname: 'texture4.jpg',
              encoding: '7bit',
              mimetype: buf.mimetype,
              buffer: buffer,
              size: Buffer.byteLength(buffer)
            },
            user: {}

          }


          resolve(req)
        })
      })
  })
}


export default user_req_generation