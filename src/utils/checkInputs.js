const { validationResult } = require("express-validator");

module.exports = function (req, res) {
  try {
    const validation_err = validationResult(req)
    if (!validation_err.isEmpty()) {
      res.status(400).json({ error: validation_err.array() });

      return false
    }
    return true

  } catch (e) {
    console.log(e);
    res.status(400).send({ error: e })
    return false


  }
}