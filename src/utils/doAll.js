const doQuery = require('../utils/doQuery')
const pool = require('../db/mysql')

module.exports = function (req, res, query) {
  try {
    doQuery(pool, query, res)
  } catch (e) {
    console.log(e);

    res.status(400).send({ error: e })

  }

}