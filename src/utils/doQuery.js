
const doQuery = function (pool, query) {
  return new Promise((resolve) => {
    try {

      pool.getConnection(async (err, cnx) => {
        if (err) throw err
        cnx.query(query,
          (err, row) => {

            cnx.release()

            if (!err && row != null) {
              if (Array.isArray(row) && !row.length > 0) resolve({ result: false, error: err })
              resolve({ result: true, query: row })
            }
            else {
              resolve({ result: false, error: err })
            }
          }

        )

      })

    } catch (e) {
      console.log(e);
      resolve({ result: false, error: e })

    }

  })


}
module.exports = doQuery