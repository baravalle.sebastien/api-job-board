module.exports = function (url_params) {
  if (url_params && url_params.offset && !isNaN(parseInt(url_params.offset))
    && parseInt(url_params.offset) > 0
  ) return parseInt(url_params.offset)
  return 0;
}