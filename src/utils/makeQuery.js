const pool = require('../db/mysql')
const { entity } = require('./parseForQuery')

module.exports = {
  query: "",
  req: "",
  values: {},
  init: function (req, values = {}) {
    this.req = req
    this.query = ""
    this.values = values
    return this
  },
  setVariables: function (vars = []) {
    vars.forEach((variable) => {
      this.query = this.query.concat(`SET ${variable[0]} = ${variable[1]}; \n`)
    })

    return this
  },

  selectFieldsOn: function (list = [], key) {
    this.query = this.query.concat('\nSELECT ')
    list.forEach(field => {
      this.query = this.query.concat('\t' + field + ',\n')
    })
    this.query = this.query.slice(0, - 2);

    this.query = this.query.concat(` FROM ${key} \n`)

    return this
  },
  selectById: function (fields, entity) {
    let SQLvar = '@' + entity;
    let id = entity + 's.id';
    let table = entity + 's'
    let placeholder = entity + '_id';
    if (entity == "company") {
      SQLvar = '@company';
      id = 'companies.id'
      table = 'companies'
      placeholder = 'company_id'
    }
    fields.push(SQLvar + ' := ' + id)

    return this.setVariables([
      [SQLvar, 'NULL']
    ]).selectFieldsOn(fields, table)
      .where([
        [id + ' = :' + placeholder, ""]
      ])

  },
  delete: function (into) {
    this.query = this.query.concat(`\nDELETE FROM ${into}\n\t`)
    return this

  },
  deleteById: function (entity, placeholder = false) {
    let id = entity + 's.id';
    let table = entity + 's'
    placeholder = placeholder === false ? ':' + entity + '_id' : placeholder;
    if (entity == "company") {
      id = 'companies.id'
      table = 'companies'
      placeholder = placeholder === false ? ':company_id' : placeholder;
    }

    return this.delete(table)
      .where([
        [id + ' = ' + placeholder, ""]
      ])

  },
  insert: function (entity, str_cat_fields = '', str_cat_values = '') {
    // Insert est une belle complexité, on lui passe le nom de l'entyté et is se charge du reste, a moins qu'on ne veuille injecter des variable SQL au quel cas on rajoute les params str_cat_fields

    let into = entity + "s"
    let fields = str_cat_fields + ':' + entity + '_insert_fields'
    let values = str_cat_values + ':' + entity + '_insert_values'
    if (entity === "company") {
      into = "companies"
    } else if (entity === "application") {
      // il n'y a pas d'autres champas a inserer que ceux passés en parametres
      // donc il ne faut pas creer de placeholder
      fields = str_cat_fields
      values = str_cat_values

    }
    this.query = this.query.concat(`\nINSERT INTO ${into}\n\t (${fields})\n\tVALUES (${values})`)
    return this

  },
  update: function (into, values) {

    this.query = this.query.concat(`\nUPDATE ${into}\n\tSET ${values} \n`)
    return this

  },
  updateById: function (entity, varSQL = undefined) {
    let id = entity + 's.id';
    let table = entity + 's'
    let placeholder = varSQL !== undefined ? varSQL : ':' + entity + '_id';
    let placeholder_update = ':' + entity + '_update'
    if (entity === "company") {
      id = 'companies.id'
      table = 'companies'
      placeholder = 'company_id'
      placeholder = varSQL !== undefined ? varSQL : ':company_id';
      placeholder_update = ':company_update'
    }

    return this.update(table, placeholder_update)
      .where([
        [id + ' = ' + placeholder, ""]
      ])

  }
  ,
  join: function (join = []) {
    if (join.length !== 0) {

      join.forEach((relation) => {
        this.query = this.query.concat(
          `\tINNER JOIN ${relation[0]} \n\t\tON ${relation[1]} \n`
        )
      });
    }
    return this

  }
  ,
  order: function (by, asc = "ASC") {

    this.query = this.query.concat(
      `\t\tORDER BY ${by} ${asc} \n`
    )
    return this

  },
  where: function (conditions = []) {
    if (conditions.length !== 0) {
      this.query = this.query.concat(
        `\t\tWHERE \n`
      )

      conditions.forEach((condition) => {
        this.query = this.query.concat(
          `\t\t\t${condition[0]} ${condition[1]}\n`
        )
      });
    }
    return this

  },
  limitAndOffset: function (limit = false, offset = false) {
    if (limit) this.query = this.query.concat(
      `\t\t\tLIMIT ${limit} `
    )
    if (offset) this.query = this.query.concat(
      `OFFSET ${offset} `
    )
    return this

  }
  ,
  close: function () {
    this.query = this.query.concat(";\n")
    return this
  },

  end: function () {
    this.query = `BEGIN; \n${this.query}\nCOMMIT; \n`
    // console.log("Requete SQL", this.query)

    return this.query;
  },
  parseParameters: function (obj = false) {
    if (!obj) {
      obj = this.values
      Object.entries(
        obj
      ).forEach(([key, property]) => {

        let re = new RegExp(':' + key, 'g')
        this.query = this.query.replace(re, property)
      })

    } else {      // cette mocherie sert a préciser la valeurs des placeholder et des nom de propriété si elles ne sont pas similaire car mon code les a forcé ex: :companie_id remplacé par company_id

      Object.entries(
        obj
      ).forEach(([key, property]) => {
        let re = new RegExp(':' + key, 'g')
        this.query = this.query.replace(re, this.values[property])
      })


    }
    return this


  },
  onCondition: function (condition = true, firstOption, secondOption) {
    if (typeof (condition) === "string") condition = this.values[condition] !== 'NULL' ? parseInt(this.values[condition]) : false


    if (condition) {
      return firstOption(this)
    } else {
      return secondOption(this)
    }

  },
  verifyPropriety: function (property) {
    let relation, SQLvar, placeholder, id;
    if (property === 'company') {
      relation = [
        ["companies", "companies.admin_id = users.id"]
      ]
      SQLvar = "@company_id"
      placeholder = ":company_id"
      id = 'companies.id'

    } else if (property === 'advertisment') {
      relation = [
        ["companies", "companies.admin_id = users.id"],
        ["advertisments", "advertisments.company_id = companies.id"]
      ]
      SQLvar = "@advertisment_id"
      placeholder = ":advertisment_id"
      id = 'advertisments.id'

    }

    return this.setVariables([[SQLvar, 'NULL']])
      .selectFieldsOn(
        [
          SQLvar + " := " + id
        ],
        "users")
      .join(
        relation
      ).where([
        // verifier la proprieté = 3 conditions

        [id + " = " + placeholder, "AND"],
        ["companies.admin_id = :user_id", "AND"],
        ["users.role != 0", ""]

      ]).close()
  },
  isAdmin: function () {
    return this.setVariables([
      ['@is_admin', 'NULL']
    ])
      .selectFieldsOn(
        ["@is_admin := users.id"], "users"
      ).where([
        [`users.id = :user_id `, "AND"],
        [`users.role != 0`, ""]
      ]).close()
  },
  isNotAdmin: function () {
    return this.setVariables([
      ['@is_not_admin', 'NULL']
    ])
      .selectFieldsOn(
        ["@is_not_admin := users.id"], "users"
      ).where([
        [`users.id = :user_id `, "AND"],
        [`users.role = 0`, ""]
      ]).close()
  }
}

