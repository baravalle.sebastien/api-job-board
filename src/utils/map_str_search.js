module.exports = function (object_to_map, cnx, entity, authorized_keys) {
  let str_search = ""
  Object.entries(
    object_to_map
  ).forEach(([key, value]) => {
    if (authorized_keys.indexOf(key) > -1) {
      str_search = str_search.concat(cnx.escapeId(key) + ' LIKE ' + cnx.escape(value + '%'), ' AND ')
    }
  })
  str_search = str_search.slice(0, - 5);

  return str_search;

}