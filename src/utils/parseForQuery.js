const pool = require('../db/mysql')

module.exports = {
  advertisment: [
    "title",
    "excerpt",
    "full_description",
    "wages",
    "working_time",
    "start_date",
    "working_place"
  ],
  company: [
    `companies.name`,
    `companies.image_url`,
    `companies.description`,
    `companies.naf`,
    `companies.siren`
  ],
  user: [
    'email',
    'first_name',
    'second_name',
    'password',
    'phone_number',
    'role',
    'image_url'
  ],
  mail: [
    'applicant_id',
    'advertisment_id',
    'is_from_applicant',
    'content',
    'date'

  ],
  userDisplayableFields: ['users.first_name', 'users.second_name',
    'users.email', 'users.phone_number', 'users.role', 'users.image_url', 'id'],
  insertValues: {},
  insertFields: {},
  updateFieldsAndValues: {},
  pool: 0,
  entity: {},
  savedProps: {

  },
  instantiate: function (object, pool) {
    this.entity = object
    this.pool = pool
    return this

  },
  setUser: function (user, user_id = false) {    // Set l'user normalement,
    //ou pour le super admin.
    if (user.role === 2 && user_id !== false) {
      return this.saveProps({
        user_id: user_id,
        super_user: user.id
      })
    }
    else {
      return this.saveProps({
        user_id: user.id
      })

    }

  },
  parseImgPath: function (file, repertory, str1, str2, property_name) {
    if (file) {
      let type = file.mimetype.split('/')
      if (type[0] === 'image') {

        let re = new RegExp(/[^a-z]/, 'gi')

        let first_str = this.entity[str1].toLowerCase()
        first_str = first_str.replace(re, 'z') + '_'
        let second_str = ''
        if (str2) {

          second_str = this.entity[str2].toLowerCase().replace(re, 's') + '_'

        }

        let path = 'img/' + repertory + '/' + first_str + second_str + Date.now().toString() + '.' + type[1]
        this.entity[property_name] = path
        this.savedProps.isPath = path;
      } else {    // un mystere a resoudre, pour une même requette, peut être y a til un cache, il n'y a pas de fichier pourtant this savedProps.isPath est toujours instancié par les requetes passés.

        delete this.savedProps.isPath;

      }
    } else {
      // un mystere a resoudre, pour une même requette, peut être y a til un cache, il n'y a pas de fichier pourtant this savedProps.isPath est toujours instancié par les requetes passés.
      delete this.savedProps.isPath;

    }
    return this

  },
  mapForInsert: function (entity) {
    let str_values = ""
    let str_fields = ""
    Object.entries(
      this.entity
    ).forEach(([key, value]) => {
      if (this[entity].indexOf(key) > -1) {
        str_fields = str_fields.concat(this.pool.escapeId(key), ', ')
        str_values = str_values.concat(this.pool.escape(value), ', ')
      }

    })
    str_values = str_values.slice(0, - 2);
    str_fields = str_fields.slice(0, - 2);
    this.savedProps[entity + '_insert_fields'] = str_fields
    this.savedProps[entity + '_insert_values'] = str_values
    return this

  },
  mapForUpdate: function (entity) {
    let str_update = ""
    Object.entries(
      this.entity
    ).forEach(([key, value]) => {
      if (this[entity].indexOf(key) > -1) {
        str_update = str_update.concat(this.pool.escapeId(key) + ' = ' + this.pool.escape(value), ', ')
      }

    })
    str_update = str_update.slice(0, - 2);
    this.savedProps[entity + '_update'] = str_update;
    return this

  },
  controlKeys: function (entity) {
    Object.entries(
      this.entity
    ).forEach(([key, value]) => {
      if (this[entity].indexOf(key) > -1) {
        this.entity[key] = value;
      }
    })
    return this

  },
  addProperties: function (propertyToAdd) {
    let object = this.entity
    Object.entries(
      propertyToAdd

    ).forEach(([key, value]) => {
      object[key] = value
    })
    this.entity = object
    return this


  }
  ,
  deleteProperties: function (propertyToDelete = false) {
    let object = this.entity
    propertyToDelete.forEach(prop => {
      this.savedProps[prop] = object[prop]
      delete object[prop]
    })
    this.entity = object
    return this
  },
  saveProps: function (propertyToSave) {
    // propriétés sauvés pour être injectés 
    Object.entries(
      propertyToSave

    ).forEach(([key, value]) => {
      this.savedProps[key] = this.pool.escape(value)
    })
    return this

  },
  giveValues: function () {
    // console.log('Valeurs Contrôlées => ', this.savedProps);

    return this.savedProps

  },
  modifyPropsIfExists: function (props, value) {
    if (this.entity[props] !== undefined) {
      this.entity[props] = value
    }

    return this

  }





}
