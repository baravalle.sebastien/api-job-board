const respond = async function (res, valueToReturn, index = undefined,
  succed = (res, row) => {
    return res.status(200).send({ query: row })

  },
  failed = (res, err) => {
    return res.status(400).send({ error: err })

  }
) {
  if (!valueToReturn || valueToReturn.result === false) {
    return failed(res, valueToReturn.error)
  }
  else if (valueToReturn.result === true) {
    let query = index !== undefined ? valueToReturn.query[index] : valueToReturn.query
    return succed(res, query)

  }
}
module.exports = respond