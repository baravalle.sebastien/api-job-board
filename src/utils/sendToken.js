const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt')

module.exports = async function (row, res, compare = false) {
  try {
    if (compare !== false) {
      const isMatch = await bcrypt.compare(compare, row.password)
      if (!isMatch) return res.status(400).send({ error: 'wrong password', code: 32 });
    }
    jwt.sign(           // Genere Token puis Repond
      {
        user: {
          id: row.insertId === undefined ? row.id : row.insertId,
          role: row.role,

        }
      }, "c'est bon",
      {
        expiresIn:
          3600 * 24 * 7
      },
      (err, token) => {
        if (err) throw err;
        res.status(200).send({ token: token, query: row });
      }
    );
  } catch (e) {
    console.error(e);
    res.status(400).send({ error: e });
  }

}
