-- DELIMITER |
-- CREATE PROCEDURE condition_process()
-- BEGIN
--   DECLARE v_count INT DEFAULT 0 ;
--         SELECT COUNT(*) FROM applications WHERE applicant_id = 4 AND advertisment_id = 1 INTO v_count;
--         IF v_count < 1 THEN 
--     START TRANSACTION;

--     INSERT INTO applications (applicant_id, advertisment_id)
--       VALUES (4, 1 );
--     INSERT INTO mails (application_id, content, is_from_applicant, date )
--       VALUES ( LAST_INSERT_ID(), 'chose', 1, '2021-09-29 12:24:10' );
    
--     COMMIT;
--   END IF;
-- END|
-- DELIMITER ;

-- CALL condition_process();
-- DROP PROCEDURE condition_process;
    
SET @v_count = 0 ;
SELECT @v_count := COUNT(*) FROM applications WHERE applicant_id = 4 AND advertisment_id = 1 ;
  START TRANSACTION;
    INSERT INTO applications (applicant_id, advertisment_id) VALUES (4, 1 );
    INSERT INTO mails (application_id, content, is_from_applicant, date ) VALUES ( LAST_INSERT_ID(), 'chose', 1, '2021-09-29 12:24:10' );
IF @v_count < 1 THEN     
  COMMIT;
END IF;